#!/usr/bin/python
from bs4 import BeautifulSoup
import urllib3, sys, datetime, re

# settings
url = 'http://www.scripts.sasg.ed.ac.uk/registry/examinations/index3.cfm'

# look up an exam using its course code
# returns a list containing, in order:
#
# name of exam (string)
# location of exam (string)
# exam start time (datetime)
# exam end time (datetime)
# duration of exam (timedelta)
def lookup(code):
	http = urllib3.PoolManager()

	post = {'school': '%%',
		  'code': code,
	  	  'course': '',
		  'searchfrm': 'yes'}

	# send search request and soupify the search results
	req = http.request('POST', url, post)
	course = BeautifulSoup(req.data)
	
	duration = re.compile('\(([0-9][0-9]):([0-9][0-9]):([0-9][0-9])\)')

	# determine exam attributes from results page
	for row in course.table.find_all('tr'):
		# remove html cruft
		row = row.get_text()

		if row.startswith(code):
			name = row[12:]
		elif row.startswith('Location:'):
			loc = row[10:]
		elif row.startswith('Date/Time:'):
			start= datetime.datetime.strptime(row[11:-20], '%A %d/%m/%Y, %H:%M:%S')
			m = duration.match(row[-10:])
			dur = datetime.timedelta(hours=int(m.group(1)), minutes=int(m.group(2)), seconds=int(m.group(3)))
			end = start + dur

	return [name,loc,start,end,dur]
	

def main(argv):
	exam = lookup(argv[0])
	
if __name__ == "__main__":
   main(sys.argv[1:])
	
